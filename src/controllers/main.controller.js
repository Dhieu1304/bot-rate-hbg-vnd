const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { Setting, User, BotMT5 } = require('../models');
const moment = require('moment');
const queryString = require('query-string');
const axios = require('axios');
const cron = require('node-cron');
const {getRateHBGtoUSDT} = require('../services/rate');

const formatTime = (date) => {
  if (date) {
    return moment(date).format('LL');
  }
};

const mainPage = catchAsync(async (req, res) => {
  console.log('vo day rồi');
  try {
    
    const setting = await Setting.findOne();

    res.render('main', { data: setting, formatTime, datas: {} });
  } catch (err) {
    res.status(400).json({ status: false, message: 'Sory, đã sảy ra lỗi', err: err.message });
  }
});

function isNumeric(str) {
  if (typeof str === 'number') return true;
  return !isNaN(str) && !isNaN(parseFloat(str));
}

const BOT_TELEGRAM = process.env.BOT_TELEGRAM;
const TELEGRAM_API = `https://api.telegram.org/bot${BOT_TELEGRAM}`;

const addUser = catchAsync(async (req, res) => {
  const setting = await Setting.findOne({});
  try {
    if (!req.body.user) {
      return res.render('main', { data: setting, formatTime });
    }
    setting.listUser.push(req.body.user);
    await setting.save();
    res.render('main', { data: setting, formatTime });
  } catch (err) {
    res.status(400).json({ status: false, message: 'Sory, đã sảy ra lỗi' });
  }
});

const getBot2Page = catchAsync(async (req, res) => {
  try {
    if (req.query.delete) {
      console.log('vo day');
      await User.deleteOne({ name: req.query.delete });
    }
    if (req.query.adduser) {
      const user = await User.findOne({ name: req.query.adduser });
      if (!user) {
        const newUser = new User({
          name: req.query.adduser,
        });
        await newUser.save();
      }
    }
    const user = await User.find({});
    const setting = await Setting.findOne({ name: 'bot-version-2' });
    res.render('main', { data: user, formatTime, datas: setting });
  } catch (err) {
    res.status(400).json({ status: false, message: 'Sory, đã sảy ra lỗi', err: err.message });
  }
});

function formatToCurrency(n, currency = '') {
  return (
    n.toFixed(0).replace(/./g, function (c, i, a) {
      return i > 0 && c !== '.' && (a.length - i) % 3 === 0 ? ',' + c : c;
    }) + currency
  );
}

const bodyFetch = (tradeType) => {
  return {
    asset: 'USDT',
    fiat: 'VND',
    page: 1,
    payTypes: [],
    publisherType: null,
    rows: 1,
    tradeType: tradeType.toUpperCase(),
  };
};
let buyPrice = 0;
let sellPrice = 0;

const fetchApi = async (isSendWhenEqual = false) => {
  try {
    // const data_HBG_USDT = await axios.get(
      // 'https://www.binance.com/bapi/composite/v1/public/promo/cmc/cryptocurrency/detail/chart?id=17382&range=1D',
    // );
    // const points = data_HBG_USDT.data.data.body.data.points;
    // const lastRateInChart = points[Object.keys(points)[Object.keys(points).length - 1]];
    // const rate = +lastRateInChart.v[0];
    const rate = await getRateHBGtoUSDT();
    console.log('rate: ', rate);
    const dataBuy = await axios.post('https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search', bodyFetch('buy'));
    const dataSell = await axios.post('https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search', bodyFetch('sell'));
    const setting = await Setting.findOne({});
    const priceHBGUSDTPlus = +setting.priceHBGUSDTPlus;
    const priceUSDTVNDPlus = +setting.priceUSDTVNDPlus;

    const buy_HBG_USDT = rate * (1 + priceHBGUSDTPlus/100);
    const sell_HBG_USDT = rate * (1 - priceHBGUSDTPlus/100);
    const buy_USDT_VND = + dataBuy.data.data[0].adv.price + priceUSDTVNDPlus;
    const sell_USDT_VND = + dataSell.data.data[0].adv.price - priceUSDTVNDPlus;
    console.log(buy_HBG_USDT ,
      sell_HBG_USDT,
      buy_USDT_VND ,
      sell_USDT_VND,
      )

    const buyPriceSession = Math.round(buy_HBG_USDT * buy_USDT_VND);
    const sellPriceSession = Math.round(sell_HBG_USDT * sell_USDT_VND);

    console.log(buyPriceSession, sellPriceSession, buyPrice, sellPrice);

    if (buyPrice !== buyPriceSession || sellPrice !== sellPriceSession || isSendWhenEqual) {
      buyPrice = buyPriceSession;
      sellPrice = sellPriceSession;
      if (setting.listUser.length === 0) {
        setting.listUser.push('@price_hbg_vnd');
        await setting.save();
      }
      const allUser = await User.find({});
      let tag = allUser.map((item) => '@' + item.name).toString();
      for (let i = 0; i < setting.listUser.length; i++) {
        let querys = queryString.stringify({
          chat_id: setting.listUser[i],
          text:
            `TỶ GIÁ KHÁCH MUA HBG: ${formatToCurrency(buyPrice)} VND/HBG.\n` +
            `TỶ GIÁ BÁN HBG:  ${formatToCurrency(sellPrice)} VND/HBG.\n` +
            '-------------------------------------------------------\n' +
            'Số lượng giao dịch tối thiểu: 100 HBG/Lệnh\n' +
            '☞ Quý khách hàng giao dịch số lượng lớn vui lòng inbox để được mức giá ưu đãi\n' +
            '☞ Hãy liên hệ nhân sự để được tư vấn cụ thể: ' + tag + ' .\n' +
            '☞ Thời gian hỗ trợ: Từ 08h00 – 23h00 từ Thứ 2 đến Chủ Nhật\n' +
            'Pi Finance hân hạnh được mang đến dịch vụ tốt nhất cho khách hàng!',
        });
        await axios.post(`${TELEGRAM_API}/sendMessage?${querys}`);
      }
    }
  } catch (err) {
    return console.error(err.message);
  }
};

function isNumeric(str) {
  if (typeof str === 'number') return true;
  return !isNaN(str) && !isNaN(parseFloat(str));
}

const botNotiRate = catchAsync(async (req, res) => {
  try {
    const { priceHBGUSDTPlus, priceUSDTVNDPlus } = req.body;
    const setting = await Setting.findOne();
    if (!isNumeric(priceHBGUSDTPlus) || !isNumeric(priceUSDTVNDPlus)) {
      return res.status(400).json({ status: false, message: 'Input is not a number' });
    }
    setting.priceHBGUSDTPlus = +priceHBGUSDTPlus;
    setting.priceHBGUSDTSub = +priceHBGUSDTPlus;
    setting.priceUSDTVNDPlus = +priceUSDTVNDPlus;
    setting.priceUSDTVNDSub = +priceUSDTVNDPlus;
    await setting.save();

    // await fetchApi();
    res.render('main', { datas: {}, formatTime, data: setting });
  } catch (err) {
    res.status(400).json({ status: false, message: 'Sorry, đã sảy ra lỗi', err: err.message });
  }
});

const botNotiRateButton = catchAsync(async (req, res) => {
  try {
    await fetchApi(true);
    const setting = await Setting.findOne();
    res.render('main', { datas: {}, formatTime, data: setting });
  } catch (err) {
    res.status(400).json({ status: false, message: 'Sorry, đã sảy ra lỗi', err: err.message });
  }
});

module.exports = {
  mainPage,
  botNotiRate,
  addUser,
  getBot2Page,
  botNotiRateButton,
};

(async () => {
  const setting = await Setting.findOne();
  if (!setting) {
    console.log('--------- init setting --------------');
    await Setting.create({
      priceHBGUSDTPlus: 1.5,
      priceHBGUSDTSub: 1.5,
      priceUSDTVNDPlus: 30,
      priceUSDTVNDSub: 30,
      listUser: ['@price_hbg_vnd'],
    });
  }

  const user = await User.findOne();
  if (!user) {
    console.log('--------- init user --------------');
    await User.create({ name: 'thangkute2608' });
  }
})();

cron.schedule('*/5 * * * *', async () => {
  await fetchApi();
});
