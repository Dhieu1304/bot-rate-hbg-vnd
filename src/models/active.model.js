const mongoose = require('mongoose');

const Active = mongoose.Schema(
  {
    name: {
      type: String,
      default: 'kyo2035',
    },
    active: {
      type: String,
      default: true,
    },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Active', Active);
