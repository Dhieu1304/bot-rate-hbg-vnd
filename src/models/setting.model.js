const mongoose = require('mongoose');

const Setting = mongoose.Schema(
  {
    priceHBGUSDTPlus: {
      type: Number,
      defaut: 1.5,
    },
    priceHBGUSDTSub: {
      type: Number,
      default: 1.5,
    },
    priceUSDTVNDPlus: {
      type: Number,
      defaut: 30,
    },
    priceUSDTVNDSub: {
      type: Number,
      default: 30,
    },
    listUser: {
      type: Array,
      default: ['@price_hbg_vnd'],
    },
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Setting', Setting);
