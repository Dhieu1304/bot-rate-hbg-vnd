const express = require('express');
const validate = require('../../middlewares/validate');
const { mainController } = require('../../controllers');

const router = express.Router();

router.route('/').get(mainController.mainPage);
router.route('/add-user').post(mainController.addUser);
router.route('/bot2').get(mainController.getBot2Page);
router.route('/').post(mainController.botNotiRate);
router.route('/send-noti').post(mainController.botNotiRateButton);

module.exports = router;
